FROM debian:buster-slim
LABEL mantainer="Hélder Ribeiro <helder.freitas.ribeiro@gmail.com>"

RUN apt-get update \
    && apt-get install -y \
        build-essential \
        cmake \
        git \
        wget \
        unzip \
        zlib1g-dev \
        libgl1-mesa-dev \
        gcc \
        g++ \
        graphviz \
        doxygen \
        gettext \
        qtscript5-dev \
        libqt5svg5-dev \
        qttools5-dev-tools \
        qttools5-dev \
        libqt5opengl5-dev \
        qtmultimedia5-dev \
        libqt5multimedia5-plugins \
        libqt5serialport5 \
        libqt5serialport5-dev \
        qtpositioning5-dev \
        libgps-dev \
        libqt5positioning5 \
        libqt5positioning5-plugins \
        qtcreator \
    && rm -rf /var/lib/apt/lists/*

RUN cd /home \
    && git clone https://gitlab.com/HFRibeiro/oastellarium.git \
    && cd /home/oastellarium \
    && mkdir -p /home/oastellarium/builds/unix \
    && cd /home/oastellarium/builds/unix \
    && cmake -DCMAKE_BUILD_TYPE="Release" ../../ \
    && make -j $(nproc) \
    && make install